<?php
require '../../lib/include.php';

header('Content-Type: application/json');

session_start();

// Check if user has been inactive for over 1.5 hrs
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 5400)) {
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time();


$response;

if (isset($_SESSION['id']) && isset($_SESSION['isTutor']) && isset($_SESSION['isStudent'])) {
    $response = array('IsLoggedIn' => true,
                      'UserId' => $_SESSION['id'],
                      'IsTutor' => $_SESSION['isTutor'],
                      'IsStudent' => $_SESSION['isStudent']
                      );
}
else {
    $response = array('IsLoggedIn' => false);
}

echo json_encode($response, JSON_FORCE_OBJECT);

?>
<?php
require '../../lib/include.php';

/*
          data.append("firstName", $("#first-name").val());
          data.append("lastName", $("#last-name").val());
          data.append("email", $("#email").val());

          data.append("tutorFirstName", $("#tutor-first-name").val());
          data.append("tutorLastName", $("#tutor-last-name").val());
          data.append("tutorEmail", $("#tutor-email").val());
          
          data.append("rating1", $("#rating1").val());
          data.append("rating2", $("#rating2").val());
          data.append("rating3", $("#rating3").val());
          data.append("rating4", $("#rating4").val());
          data.append("rating5", $("#rating5").val());
          data.append("rating6", $("#rating6").val());
          data.append("review", $("#review").val());

			^data sent
*/

$post=$_POST;
$first = setPost($post,"firstName","First name");
$last = setPost($post,"lastName","Last name");
$email= setPost($post,"email","Email");

$tutorFirst= setPost($post,"tutorFirstName","Tutor's First Name");
$tutorLast= setPost($post,"tutorLastName","Tutor's Last Name");
$tutorEmail= setPost($post,"tutorEmail","Tutor's Email");

$availability = setPost($post,"availability","Rating for Availability");
$knowledge = setPost($post,"knowledge","Rating for Knowledge of Course Material");
$ability = setPost($post,"ability","Rating for Ability to Explain Concepts");
$timeliness = setPost($post,"timeliness","Rating for Timeliness");
$productivity = setPost($post,"productivity","Rating for Productive Session");
$receptivity = setPost($post,"receptivity","Rating for Receptive and Communicative");

$review = setPost($post,"review","Review", false);


//TODO: get userID of reviewer
$stmt = $mysqli->prepare("select UserID from Users where Email = ?");
$stmt->bind_param('s', $email);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$stmt->bind_result($userId);

$stmt->fetch();

$stmt->close();

//TODO: get ID of tutor with this email
$stmt = $mysqli->prepare("select UserID from Users where Email = ? and IsTutor=1");
$stmt->bind_param('s', $tutorEmail);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$stmt->bind_result($tutorId);

$stmt->fetch();

$stmt->close();

if(!$tutorId) {
	shareSuccess(false,"Tutor does not exist");
}

else if(!$userId) {
	shareSuccess(false, "Not an existing user");
}

else{

	$result=Review::MakeNewReview($userId, $tutorId, $review, $availability, $knowledge, $ability, $timeliness, $productivity, $receptivity);
	$message = $result[0];
	$userId = $result[1];
	shareSuccess(true,$message);
}

function setPost($post,$name,$errorName,$mandatory=true){
	//if field is not optional, exit page
	if(isset($post[$name])&&!empty($post[$name])){
		return $post[$name];
	}
	else if($mandatory){
		//STARTED WORKING ON ADD TUTOR FUNCTION IN CLASSES, PASS THIS INFORMATION THERE
		shareSuccess(false,$errorName." field is missing.");
	}
	else{
		return "";
	}
}

function shareSuccess($Success,$Message){
	//always end with this function
	$returnData=array();
	$returnData["Success"]=$Success;
	$returnData["Message"]=$Message;
	header('Content-Type: application/json');
	echo json_encode($returnData);
	exit;
}

?>

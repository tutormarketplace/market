<?php
require '../../lib/include.php';

/*
 *      data.append("classId", $("#class").val());
        data.append("date", $("#date").val());
        data.append("rate", $("#rate").val());
        data.append("duration", $("#num-hours").val());
        data.append("total", $("#amount-owed").val());
        data.append("firstName", $("#student-first-name").val());
        data.append("lastName", $("#student-last-name").val());
        data.append("email", $("#student-email").val());
            ^
        data sent
  */

$post=$_POST;
$classId = setPost($post, "classId", "Class");
$date = setPost($post, "date", "Date");
$rate = setPost($post, "rate", "Rate");
$duration = setPost($post, "duration", "Duration of Session");
$totalAmountOwed = setPost($post, "total", "Total Amount Owed");
$studentFirst = setPost($post,"firstName","First name");
$studentLast = setPost($post,"lastName","Last name");
$studentEmail= setPost($post,"email","Email");

session_start();
if (!isset($_SESSION['id']) || !isset($_SESSION['isTutor']) || !isset($_SESSION['isStudent'])) {
    shareSuccess(false, "Please Sign In Again.");
}
elseif(!$_SESSION["isTutor"]) {
    shareSuccess(false, "You must be a tutor to submit session forms.");
}

$tutorId = $_SESSION['id'];

//check if student is already a registered user
$stmt = $mysqli->prepare("select UserId from Users where Email = ?");
$stmt->bind_param('s', $studentEmail);

if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}

$stmt->execute();
$stmt->bind_result($studentUserId);
$stmt->fetch();
$stmt->close();

if(empty($studentUserId)) {
    //add new student user if they aren't already registered
    $result = Student::MakeNewStudent($studentFirst, $studentLast, $studentEmail);
    $message = $result[0];
    $studentUserId = $result[1];
}

//add tutoring session
$result = Session::MakeNewSession($classId, $tutorId, $studentUserId, $date, $rate, $duration, $totalAmountOwed);
$message = $result[0];
$sessionId = $result[1];

//email session form to student
$session = Session::FromId($sessionId);
$emailMessage = sendSessionFormToStudent($session);

shareSuccess(true, $message . " " . $emailMessage);


function setPost($post,$name,$errorName,$mandatory=true){
    //if field is not optional, exit page
    if(isset($post[$name])&&!empty($post[$name])){
        return $post[$name];
    }
    else if($mandatory){
        shareSuccess(false,$errorName." field is missing.");
    }
    else{
        return "";
    }
}

function shareSuccess($Success,$Message){
    //always end with this function
    $returnData=array();
    $returnData["Success"]=$Success;
    $returnData["Message"]=$Message;
    header('Content-Type: application/json');
    echo json_encode($returnData);
    exit;
}

?>
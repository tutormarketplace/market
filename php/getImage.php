<?php
require '../../lib/include.php';

if(is_null($_GET["userId"])){
	echo "Invalid post parameters";
	exit;
}

$userId=$_GET["userId"];
$path = "../../images/".$userId;
if(file_exists($path)) {
	echo base64_encode(file_get_contents($path));
}
else {
	error_log("Image file for user with id ".$userId." doesn't exist");
	echo "";
}
?>

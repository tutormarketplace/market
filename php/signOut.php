<?php
require '../../lib/include.php';

header('Content-Type: application/json');

session_start();
session_unset();     // unset $_SESSION variable for the run-time 
session_destroy();   // destroy session data in storage

$_SESSION['LAST_ACTIVITY'] = time();

echo json_encode(array("Success" => true));

?>
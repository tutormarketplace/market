<?php
require '../../lib/include.php';

$post=$_POST;
$email= setPost($post,"email","Email");
$password= setPost($post,"password","Password");
global $hash;
$password=crypt($password,$hash);


$query="select UserId, IsTutor, IsStudent from Users where Email=? and Password=?";
$stmt = $mysqli->prepare($query);
$stmt->bind_param('ss', $email,$password);
$stmt->execute();
$stmt->bind_result($UserId,$isTutor, $isStudent);


while ($stmt->fetch()) {
    session_start();
    $_SESSION['id'] = $UserId;
    $_SESSION['isTutor'] = $isTutor;
    $_SESSION['isStudent'] = $isStudent;
    shareSuccess(true,"Successfully logged in",$UserId,$isTutor,$isStudent);
}
shareSuccess(false,"Wrong username or password");

function setPost($post,$name,$errorName,$mandatory=true){
    //if field is not optional, exit page
    if(isset($post[$name])&&!empty($post[$name])){
        return $post[$name];
    }
    else if($mandatory){
        shareSuccess(false,$errorName." field is missing.");
    }
    else{
        return "";
    }
}
function shareSuccess($Success,$Message,$UserId="",$isTutor=false,$isStudent=false){
    //always end with this function
    $returnData=array();
    $returnData["Success"]=$Success;
    $returnData["Message"]=$Message;
    $returnData["UserId"]=$UserId;
    $returnData["isTutor"]=$isTutor;
    $returnData["isStudent"]=$isStudent;
    header('Content-Type: application/json');
    echo json_encode($returnData);
    exit;
}
?>
<?php
require '../../lib/include.php';
if(is_null($_GET["TutorId"])){
	echo "Invalid post parameters";
	exit;
}
$TutorId=$_GET["TutorId"];

$tutor=Tutor::FromIdOnlyApproved($TutorId);
header('Content-Type: application/json');
echo json_encode($tutor);
?>


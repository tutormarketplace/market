<?php
require '../../lib/include.php';

/*data.firstName=$("#first-name").val();
            data.lastName=$("#last-name").val();
            data.email=$("#email").val();
            data.password=$("#password").val();
            data.messangerLink=$("#facebook").val();
            data.school=$("#school").val();
            data.major=$("#major").val();
            data.year=$("#year").val();
            data.class1=$("#class1").val();
            data.school1=$("#school1").val();
            data.grade1=$("#grade1").val();
            data.class2=$("#class2").val();
            data.school2=$("#school2").val();
            data.grade2=$("#grade2").val();
            data.class3=$("#class3").val();
            data.school3=$("#school3").val();
            data.grade3=$("#grade3").val();

			^data sent
*/

$post=$_POST;
$first = setPost($post,"firstName","First name");
$last = setPost($post,"lastName","Last name");
$email= setPost($post,"email","Email");
$password= setPost($post,"password","Password");
$messengerLink= setPost($post,"messengerLink","Facebook Messenger link", false);
$school = setPost($post,"school","School");
$major = setPost($post,"major","Major");
$year = setPost($post,"year","Year");
$bio = setPost($post,"bio","Bio");
$pictureFile = setPost($post,"pictureFile", "Profile Image");
$classes = json_decode(setPost($post,"classes","Class Information"), true);

//Check if email is already used

$stmt = $mysqli->prepare("select count(*) from Users where Email = ?");
$stmt->bind_param('s', $email);
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$stmt->bind_result($count);

$stmt->fetch();

$stmt->close();
if($count == 1) {
	//used
	shareSuccess(false,"Email already used");
}
else{
	//success, add tutor here
	$result=Tutor::MakeNewTutor($first,$last,$email,$password,$school,$major,$year,$classes,$messengerLink,$bio);
	$message = $result[0];
	$userId = $result[1];
	uploadImage($pictureFile, $userId);
	sendApplicationNotificationToAdmin(Tutor::FromId($userId));
	shareSuccess(true,$message);
}

function setPost($post,$name,$errorName,$mandatory=true){
	//if field is not optional, exit page
	if(isset($post[$name])&&!empty($post[$name])){
		return $post[$name];
	}
	else if($mandatory){
		//STARTED WORKING ON ADD TUTOR FUNCTION IN CLASSES, PASS THIS INFORMATION THERE
		shareSuccess(false,$errorName." field is missing.");
	}
	else{
		return "";
	}
}
function shareSuccess($Success,$Message){
	//always end with this function
	$returnData=array();
	$returnData["Success"]=$Success;
	$returnData["Message"]=$Message;
	header('Content-Type: application/json');
	echo json_encode($returnData);
	exit;
}

function uploadImage($image,$userId){
	$target_file = "../../images/".$userId;
	$imageData = base64_decode($image);
	$source = imagecreatefromstring($imageData);
	imagejpeg($source,$target_file,100);
}

?>

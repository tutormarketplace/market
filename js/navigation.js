function addLoggedInNavItems(userId) {
    var newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "SessionForm.html");
    newNavItemElement.find('a').text("Session Form");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "Market.html");
    newNavItemElement.find('a').text("Tutors");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "TutoringGuidelines.html");
    newNavItemElement.find('a').text("Tutoring Guidelines");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "FAQ.html");
    newNavItemElement.find('a').text("FAQs");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "#contact");
    newNavItemElement.find('a').text("Contact Us");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    $.ajax({
        url: './php/getIndividualTutor.php',
        data: { "TutorId": userId },
        dataType: 'json',
        success: function(data)
        {
            var tutor = new Tutor(data);
            newNavItemElement = $("#base-user-nav-item").clone();
            newNavItemElement.attr('id', 'user-nav-item');
            newNavItemElement.find('#name').text(tutor.firstName);
            newNavItemElement.find('#profile').attr("href", "TutorProfile.html?id=" + tutor.userId);
            newNavItemElement.show();
            $("#nav-items").append(newNavItemElement);
        }
    });
}

function addLoggedOutNavItems() {
    var newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "Market.html");
    newNavItemElement.find('a').text("Find A Tutor");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "BeATutor.html");
    newNavItemElement.find('a').text("Be A Tutor");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "TutoringGuidelines.html");
    newNavItemElement.find('a').text("Tutoring Guidelines");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "FAQ.html");
    newNavItemElement.find('a').text("FAQs");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "#contact");
    newNavItemElement.find('a').text("Contact Us");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
    
    newNavItemElement = $("#base-nav-item").clone();
    newNavItemElement.attr('id', 'nav-item');
    newNavItemElement.find('a').attr('href', "SignIn.html");
    newNavItemElement.find('a').text("Sign In");
    newNavItemElement.show();
    $("#nav-items").append(newNavItemElement);
}

function signOut() {
    $.get("./php/signOut.php");
    location.reload();
}

(function($) {
    "use strict"; // Start of use strict
    $(function(){
        $("nav").load("templates/navigation.html", function(){
            $("#base-nav-item").hide();
            $("#base-user-nav-item").hide();
           
            $.ajax({
               url: './php/isSignedIn.php',
               data: "",
               dataType: 'json',
               success: function(data)
               {
                 if (!data.IsLoggedIn) {
                     addLoggedOutNavItems();
                 }
                 else {
                   addLoggedInNavItems(data.UserId);
                 }
               }
           });
        });
    });
})(jQuery); // End of use strict
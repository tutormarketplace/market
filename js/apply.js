function bindSubmit() {
  $( "#submit" ).click(function() {
      if(!$("#permission").prop("checked")){
	alert("Please check the box granting us permission to verify your information. We cannot add you if you do not do so.");
	return;
      }
      var data=new FormData();
      data.append("firstName", $("#first-name").val());
      data.append("lastName", $("#last-name").val());
      data.append("email", $("#email").val());
      data.append("password", $("#password").val());
      if (!$("#no-facebook").prop("checked")) {
	data.append("messengerLink", $("#facebook").val());
      }
      data.append("school", $("#school").val());
      data.append("major", $("#major").val());
      data.append("year", $("#year").val());

      var classData = [];
      $.each($("#class-rows").children(), function(index, el) {
        if ($(el).find("#class").attr("data-classid") == undefined && $(el).find("#class").val() != "") {
          var _class = $(el).find("#class").val();
        }
        // If the class does not currently exist in the database
        else {
          var _class = $(el).find("#class").attr("data-classid");
        }
        classData.push({
          "class": _class,
          "school": $(el).find("#school").val(),
          "grade": $(el).find("#grade").val()
        });
      });
      data.append("classes", JSON.stringify(classData));

      data.append("bio", $("#bio").val());
      if($('.fileinput').fileinput().find('img').length == 0) {
        alert("Please enter a jpg or png image");
        return;
      }
      else {
        data.append("pictureFile", $('.fileinput').fileinput().find('img')[0].src.split(",")[1]);
      }
      $.ajax({
          type: "POST",
          url: 'php/tutorApplication.php',
          contentType: false,
          processData: false,
          data: data,
          success: function(response){alert(response.Message);}
      });
  });
}

function bindMajors() {
  var majorsData = null;
  $.getJSON( "/json/majors.json", function(data) {
    majorsData = data;
  });

  $("#school").change(function(){
    $("#major").autocomplete({
       source: majorsData[$("#school").val()]
     });
  });
}

function getAllClasses() {
    $.ajax({
        type: "POST",
        url: 'php/getAllClasses.php',
        success: function(data){
            $.each(data, function(index, classData) {
              classes.push(classData);
            });
        }
    });
}

function bindClassRows() {
  // Default is 3 class rows
  bindClassData($("#class-row"));
  bindNewClassRow();
  bindNewClassRow();

  $("#add-more-classes").click(function(){
    bindNewClassRow();
  });
}

function bindNewClassRow(el) {
  var classRow = $("#class-row").clone();
  bindClassData(classRow);
  $("#class-rows").append(classRow);
}

function bindClassData(classRow) {
  $(classRow).find("#school").change(function(){
    var school = this.value;
    var classValues = [];
    $.each(classes, function(index, _class) {
        if (_class.College == school) {
          classValues.push({
            id: _class.ClassId,
            label: _class.OfficialIdentifier + ": " + _class.OfficialName
          });
        }
    });

    $(classRow).find("#class").autocomplete({
       source: classValues,
       select: function(event, ui) {
         $(classRow).find("#class").attr("data-classid", ui.item.id);
       }
     });
  });
}

function bindNoFacebook() {
  $("#no-facebook").change(function(){
    if ($("#no-facebook").prop("checked")) {
      $("#facebook").attr("readonly", true);
    }
    else {
      $("#facebook").attr("readonly", false);
    }
  });
}

var classes = [];

(function($) {
  "use strict"; // Start of use strict
  $(function() {
    bindSubmit();
    bindMajors();
    getAllClasses();
    bindClassRows();
    bindNoFacebook();
    $('[data-toggle="tooltip"]').tooltip();
  });
})(jQuery); // End of use strict

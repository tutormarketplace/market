function bindTutorToDOM(tutor) {
  if($('[id="tutor-row"]').length == 0
        || $('[id="tutor-row"]').last().children('[id="tutor-element"]').length == 3) {
    $("#tutor-list").append($("#base-tutor-row").clone().attr("id", "tutor-row"));
  }
  var newTutorElement = $('#base-tutor-element').clone();
  newTutorElement.attr('id', 'tutor-element');
  newTutorElement.attr('data-userId', tutor.userId);
  newTutorElement.find('#name').text(tutor.getFullName());
  newTutorElement.find('#name').click(function(){
    tutorProfileRedirect(newTutorElement.attr('data-userId'));
  });
  var numberOfReviewsElement = newTutorElement.find('#overall-rating').find('#number-of-reviews');
  if (tutor.numberOfRatings == 1) {
      numberOfReviewsElement.text(tutor.numberOfRatings + " review");
  }
  else {
      numberOfReviewsElement.text(tutor.numberOfRatings + " reviews");
  }
  newTutorElement.find('#overall-rating').html(tutor.getRatingStars());
  newTutorElement.find('#overall-rating').append(numberOfReviewsElement);
  tutor.bindImageToDOM(newTutorElement.find('#profile-image'));
  if (tutor.messengerLink == "") {
    newTutorElement.find('#messenger-link').hide();
    newTutorElement.find('#email-link').attr('href', "mailto:" + tutor.email + "?Subject=Tutor Marketplace Tutoring");
    newTutorElement.find('#email-link').click(function() {
      ga('send', 'event', "message", "email-message", tutor.userId);
    });
  }
  else {
    newTutorElement.find('#email-link').hide();
    newTutorElement.find('#messenger-link').attr('href', tutor.messengerLink);
    newTutorElement.find('#messenger-link').click(function() {
      ga('send', 'event', "message", "fb-message", tutor.userId);
    });
  }

  tutor.bindClassesToDOM(newTutorElement.find('#base-class-container'));

  $('[id="tutor-row"]').last().append(newTutorElement);

  tutorsShown.push(tutor);
  return newTutorElement;
}

function removeTutorFromDOM(tutor) {
  var parentOfRemoved = $("[data-userId='" + tutor.userId+"']").parent();
  $("[data-userId='" + tutor.userId+"']").remove();

  while(parentOfRemoved.index() != $('#tutor-list').children().length - 1) {
    parentOfRemoved.append(parentOfRemoved.next().children('#tutor-element').first().detach());
    parentOfRemoved = parentOfRemoved.next();
  }

  if (parentOfRemoved.children('#tutor-element').length == 0) {
    parentOfRemoved.remove();
  }

  if($('#tutor-element').length == 0) {
    $('#no-tutors').show();
  }

  tutorsNotShown.push(tutor);
}

function tutorProfileRedirect(userId) {
  window.location.href = "TutorProfile.html?id=" + userId;
}

function getAllTutorsAndBind() {
  $.ajax({
      url: './php/getTutors.php',
      data: "",
      dataType: 'json',
      success: function(data)
      {
        $.each(data, function(index, tutorJson) {
          var tutor = new Tutor(tutorJson);
          bindTutorToDOM(tutor);
        });
      }
  });
}

function bindSearchBar() {
  var searchBar = $('#search-bar');
  searchBar.keyup(function() {
    //Google Analytics track what is being searched
    ga('send', 'event', "search", searchBar.val());
    filterTutorsInDOM();
  });
}

function bindFilters() {
  var basePriceFilterButton = $('#filters').find('#base-price-filter');
  $.each(PriceRanges.getRateStrings(), function(index, rateString) {
    var priceFilterButton = basePriceFilterButton.clone();
    priceFilterButton.attr('id', "price-filter");
    priceFilterButton.text(rateString);
    priceFilterButton.attr('title', PriceRanges.getRangeStringMap()[rateString]);
    priceFilterButton.click(function(){
      //filter has been deactivated
      if(priceFilterButton.hasClass('btn-primary')) {
        priceFilterButton.removeClass('btn-primary');
        priceFilterButton.addClass('active');
        priceFilterButton.addClass('btn-default');
      }
      //filter has been activated
      else if(priceFilterButton.hasClass('btn-default')) {
        priceFilterButton.removeClass('btn-default');
        priceFilterButton.removeClass('active');
        priceFilterButton.addClass('btn-primary');
      }
      filterTutorsInDOM();
    });
    basePriceFilterButton.before(priceFilterButton);
  });
  basePriceFilterButton.remove();
}

function filterTutorsInDOM() {
  tutorsShown = $.grep(tutorsShown, function(tutor, index) {
    if(tutor.isInPriceRange(PriceRanges.getActiveRates()) && tutor.containsClass($('#search-bar').val())) {
      return true;
    }
    else {
      removeTutorFromDOM(tutor);
      return false;
    }
  });

  tutorsNotShown = $.grep(tutorsNotShown, function(tutor, index) {
    if(tutor.isInPriceRange(PriceRanges.getActiveRates()) && tutor.containsClass($('#search-bar').val())) {
      bindTutorToDOM(tutor);
      $('#no-tutors').hide();
      return false;
    }
    else {
      return true;
    }
  });
}

var tutorsShown = [];
var tutorsNotShown = [];

(function($) {
  "use strict"; // Start of use strict
  $(function() {
    bindSearchBar();
    bindFilters();
    getAllTutorsAndBind();
    $('[data-toggle="tooltip"]').tooltip();
  });
})(jQuery); // End of use strict

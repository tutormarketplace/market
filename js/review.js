function bindTutorToDOM(tutor) {
  var tutorReviewElement = $("#tutor-info");
  tutorReviewElement.find('#tutor-first-name').attr('value', tutor.firstName);
  tutorReviewElement.find('#tutor-first-name').attr('placeholder', tutor.firstName);
  tutorReviewElement.find('#tutor-last-name').attr('value', tutor.lastName);
  tutorReviewElement.find('#tutor-last-name').attr('placeholder', tutor.lastName);
  tutorReviewElement.find('#tutor-email').attr('value', tutor.email);
  tutorReviewElement.find('#tutor-email').attr('placeholder', tutor.email);

  tutorReviewElement.find('#tutor-first-name').attr('readOnly', true);
  tutorReviewElement.find('#tutor-last-name').attr('readOnly', true);
  tutorReviewElement.find('#tutor-email').attr('readOnly', true);
}

function bindStudentToDOM(firstName, lastName, email) {
  var tutorReviewElement = $("#student-info");
  tutorReviewElement.find('#first-name').attr('value', firstName);
  tutorReviewElement.find('#first-name').attr('placeholder', firstName);
  tutorReviewElement.find('#last-name').attr('value', lastName);
  tutorReviewElement.find('#last-name').attr('placeholder', lastName);
  tutorReviewElement.find('#email').attr('value', email);
  tutorReviewElement.find('#email').attr('placeholder', email);
}

function getTutorAndBind(userId) {
  $.ajax({
      url: './php/getIndividualTutor.php',
      data: { "TutorId": userId },
      dataType: 'json',
      success: function(data)
      {
        var tutor = new Tutor(data);
        bindTutorToDOM(tutor);
      }
  });
}

function bindSubmit() {
  $( "#submit" ).click(function() {
      var data=new FormData();
      data.append("firstName", $("#first-name").val());
      data.append("lastName", $("#last-name").val());
      data.append("email", $("#email").val());

      data.append("tutorFirstName", $("#tutor-first-name").val());
      data.append("tutorLastName", $("#tutor-last-name").val());
      data.append("tutorEmail", $("#tutor-email").val());

      data.append("availability", $("#rating1").val());
      data.append("knowledge", $("#rating2").val());
      data.append("ability", $("#rating3").val());
      data.append("timeliness", $("#rating4").val());
      data.append("productivity", $("#rating5").val());
      data.append("receptivity", $("#rating6").val());
      data.append("review", $("#review").val());

      $.ajax({
          type: "POST",
          url: 'php/leaveReview.php',
          contentType: false,
          processData: false,
          data: data,
          success: function(response){alert(response.Message);}
      });
  });
}

(function($) {
  "use strict"; // Start of use strict
  $(function(){
    if (window.location.href.indexOf("&first=") != -1) {
      var dataIndex = window.location.href.indexOf("id=") + "id=".length;
      var userId = window.location.href.substring(dataIndex, window.location.href.indexOf("&first="));
      getTutorAndBind(userId);
      
      dataIndex = window.location.href.indexOf("first=");
      var studentFirstName = window.location.href.substring(dataIndex + "first=".length, window.location.href.indexOf("&last="));
      dataIndex = window.location.href.indexOf("last=");
      var studentLastName = window.location.href.substring(dataIndex + "last=".length, window.location.href.indexOf("&email="));
      dataIndex = window.location.href.indexOf("email=");
      var studentEmail = window.location.href.substring(dataIndex + "email=".length);
      bindStudentToDOM(studentFirstName, studentLastName, studentEmail);
    }
    else if (window.location.href.indexOf("=") != -1) {
      var userId = window.location.href.substring(window.location.href.indexOf("=") + 1);
      getTutorAndBind(userId);
    }

    bindSubmit();
  });
})(jQuery); // End of use strict

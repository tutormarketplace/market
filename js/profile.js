function bindTutorToDOM(tutor) {
  var tutorHeaderElement = $("#tutor-header");
  tutorHeaderElement.find('#name').text(tutor.getFullName());
  tutorHeaderElement.find('#year-school').text(tutor.year + " | " + tutor.school + " | " + tutor.major);

  var tutorBodyElement = $("#tutor-body");
  tutor.bindImageToDOM(tutorBodyElement.find('#profile-image'));
  tutorBodyElement.find('#bio').text(tutor.bio);
  tutor.bindClassesToDOM(tutorBodyElement.find('#base-class-container'));

  $.each(tutorBodyElement.find("[id=email-link]"), function(index, el){
    $(el).attr('href', "mailto:" + tutor.email + "?Subject=Tutor Marketplace Tutoring");
    $(el).click(function() {
      ga('send', 'event', "message", "email-message", tutor.userId);
    });
  });
  
  if (tutor.messengerLink == "") {
    $.each(tutorBodyElement.find("[id=messenger-link]"), function(index, el){
      $(el).hide();
    });
  }
  else {
    $.each(tutorBodyElement.find("[id=messenger-link]"), function(index, el){
      $(el).attr('href', tutor.messengerLink);
      $(el).click(function() {
        ga('send', 'event', "message", "fb-message", tutor.userId);
      });
    });
  }


  var tutorBodyElement = $("#tutor-reviews");
  //fill in with reviews
  tutorBodyElement.find('#review-link').click(function(){
    tutorReviewRedirect(tutor.userId);
  });
}

function bindReviewToDOM(review) {
  var newReviewElement = $('#base-review-element').clone();
  newReviewElement.attr('id', 'review-element');
  newReviewElement.attr('data-reviewId', review.reviewId);

  newReviewElement.find('#review-content').text(review.review);

  newReviewElement.find('#review-author').text("-" + review.authorName);

  $('#base-review-element').before(newReviewElement);

  return newReviewElement;
}

function getAllReviewsAndBind(userId) {
  $.ajax({
      url: './php/getReviews.php',
      data: {"id": userId },
      dataType: 'json',
      success: function(data)
      {

        if(!jQuery.isEmptyObject(data)){
              $.each(data, function(index, reviewJson) {
          var review = new Review(reviewJson);
          previousTutorElement = bindReviewToDOM(review);
          });
        }
        else {
          $('#no-reviews').text("No reviews yet!");

        }

      }
  });
}

function getTutorAndBind(userId) {
  $.ajax({
      url: './php/getIndividualTutor.php',
      data: { "TutorId": userId },
      dataType: 'json',
      success: function(data)
      {
        var tutor = new Tutor(data);
        bindTutorToDOM(tutor);
      }
  });
}

function tutorReviewRedirect(userId) {
  window.location.href = "LeaveReview.html?id=" + userId;
}

(function($) {
  "use strict"; // Start of use strict
  $(function(){
    if (window.location.href.indexOf("=") == -1) {
      //Tutor does not exist
      window.location.href = "Market.html";
      return;
    }
    var userId = window.location.href.substring(window.location.href.indexOf("=") + 1).replace(/\D/g,'');
    getTutorAndBind(userId);
    getAllReviewsAndBind(userId);
  });
})(jQuery); // End of use strict

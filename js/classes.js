class Tutor {
  constructor(data) {
    this.userId = data['UserId'];
    this.isBanned = data['IsBanned'];
    this.bio = data['Bio'];
    this.overallRating = data['OverallRating'];
    this.classes = setClasses(data['Offerings']);
    this.numberOfRatings = getTotalNumberOfRatings(this.classes);
    this.phone = data['Phone'];
    this.firstName = data['FirstName'];
    this.lastName = data['LastName'];
    this.email = data['Email'];
    this.messengerLink = data['MessengerLink'];
    this.year = data['Year'];
    this.school = data['College'];
    this.major = data['Major'];
  }

  getFullName() {
    return this.firstName + " " + this.lastName;
  }

  containsClass(classString) {
    var contains = false;
    $.each(this.classes, function(index, _class) {
      if(_class.containsName(classString)) {
        contains = true;
        return;
      }
    });
    return contains;
  }

  bindImageToDOM(imageElement) {
    $.ajax({
        url : './php/getImage.php',
        data: {'userId': this.userId},
        dataType: 'json',
    }).always(function(b64data){
       imageElement.attr('src', "data:image/jpeg;base64," + b64data.responseText);
    });
  }

  bindClassesToDOM(classContainerElement) {
    $.each(this.classes, function(index, classObject) {
      var newClassContainerElement = classContainerElement.clone();
      newClassContainerElement.attr('id', "class-container");
      newClassContainerElement.attr('data-classId', classObject.classId);
      newClassContainerElement.find('#class').text(classObject.informalName);
      newClassContainerElement.find('#rate').text(classObject.getRateString());
      newClassContainerElement.find('#rate').attr("title", classObject.getFullRateString());
      newClassContainerElement.find('#full-rate').text(classObject.getFullRateString());
      newClassContainerElement.find('#rating').html(classObject.getRatingStars() + newClassContainerElement.find('#rating').html());
      if (classObject.numberOfRatings == 1) {
        newClassContainerElement.find('#number-of-reviews').text(classObject.numberOfRatings + " review");
      }
      else {
        newClassContainerElement.find('#number-of-reviews').text(classObject.numberOfRatings + " reviews");
      }

      classContainerElement.before(newClassContainerElement);
    });
    classContainerElement.remove();
  }

  getRatingStars(){
    return getRatingStarsFromRating(this.overallRating);
  }

  isInPriceRange(ratingStrings) {
    if (ratingStrings.length == 0 || ratingStrings.length == PriceRanges.getRateStrings().length) {
      return true;
    }

    var isInRange = false;
    $.each(this.classes, function(index, classObject) {
      if(ratingStrings.indexOf(classObject.getRateString()) != -1) {
        isInRange = true;
        return;
      }
    });
    return isInRange;
  }
}

function getRatingStarsFromRating(rating) {
  var html = "";
  var i = 0;
  while(i < Math.floor(rating/2)) {
    html += '<i class="glyphicon glyphicon-star"></i>';
    i++;
  }

  //half star on odd numbers
  if(rating % 2 == 1){
    html+= '<i class="glyphicon glyphicon-star half"></i>';
    i++;
  }

  while(i < 5) {
    html+= '<i class="glyphicon glyphicon-star empty"></i>';
    i++;
  }
  return html;
}

function setClasses(data) {
  var classes = [];
  $.each(data, function(index, offeringJson) {
     classes.push(new ClassOffering(offeringJson));
  });
  return classes;
}

function getTotalNumberOfRatings(classes) {
  var num = 0;
  $.each(classes, function(index, classObject) {
     num += classObject.numberOfRatings;
  });
  return num;
}

class ClassOffering {
  constructor(data) {
    this.classId = data['ClassId'];
    this.approved = data['Approved'];
    this.grade = data['Grade'];
    this.rate = data['Rate'];
    this.rating = data['Rating'];
    this.numberOfRatings = data['NumberOfRatings'];
    this.officialIdentifier = data['OfficialIdentifier'];
    this.officialName = data['OfficialName'];
    this.informalName = data['InformalName'];
  }

  containsName(classString) {
    return classString == ""
            || this.officialName.toLowerCase().indexOf(classString.toLowerCase()) !== -1
            || this.informalName.toLowerCase().indexOf(classString.toLowerCase()) !== -1
            || this.officialIdentifier.toLowerCase().indexOf(classString.toLowerCase()) !== -1;
  }

  getRatingStars(){
    return getRatingStarsFromRating(this.rating);
  }

  getRateString() {
    return PriceRanges.getRateString(this.rate);
  }

  getFullRateString() {
    return PriceRanges.getRangeStringMap()[this.getRateString(this.rate)];
  }
}

//incomplete doesnt include segmented ratings...
class Review {
  constructor(data) {
    this.reviewId = data['ReviewId'];
    this.authorId = data['AuthorId'];
    this.tutorId = data['TutorId'];
    this.review = data['Review'];
    this.authorName = data['AuthorName'];
  }
}

class PriceRanges {
  static getRateStrings() {
      return ['$', '$$', '$$$', '$$$$', '$$$$$'];
  }

  static getRangeStringMap() {
    var rangeStringMaps = new Object();
    rangeStringMaps['$'] = "$10-14";
    rangeStringMaps['$$'] = "$15-19";
    rangeStringMaps['$$$'] = "$20-24";
    rangeStringMaps['$$$$'] = "$25-29";
    rangeStringMaps['$$$$$'] = "$30-40";
    return rangeStringMaps;
  }

  static getRateString(rate) {
    if(rate < 15) {
      return '$';
    }
    else if(rate < 20) {
      return '$$';
    }
    else if(rate < 25) {
      return '$$$';
    }
    else if(rate < 30) {
      return '$$$$';
    }
    else { // >=30
      return '$$$$$';
    }
  }

  static getActiveRates() {
    var activeRates = []
    $.each($('button[id=price-filter]'), function(index, el) {
      if(el.classList.contains('btn-primary')) {
        var rateString = el.textContent;
        activeRates.push(rateString);
      }
    });
    return activeRates;
  }
}

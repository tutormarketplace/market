function bindSubmitButtons() {
    //submit on enter press of password field
    $("#password").keypress(function (ev) {
        var keycode = (ev.keyCode ? ev.keyCode : ev.which);
        if (keycode == '13') {
            submit();
        }
    });
    
    //submit on click of submit button
    $("#signin-btn").click(function() {
        submit();
    });
}

function submit() {
    var email = $("#email").val();
    var password = $("#password").val();

    $.ajax({
        type: 'POST',
        url: './php/signIn.php',
        data: {"email": email, "password": password },
        dataType: 'json',
        success: function(response)
        {
            if(response.Success) {
                if(response.isTutor) {
                    window.location.href = "SessionForm.html";
                }
            }
            else {
                alert(response.Message);
            }
        }
    });
}

(function($) {
    "use strict"; // Start of use strict
    $(function(){
        bindSubmitButtons();
    });
})(jQuery); // End of use strict
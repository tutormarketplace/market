function bindSubmit() {
    $( "#submit" ).click(function() {
        var data=new FormData();  
        data.append("classId", $("#class").val());
        data.append("date", $("#date").val());
        data.append("rate", $("#rate").val());
        data.append("duration", $("#num-hours").val());
        data.append("total", $("#amount-owed").val());
        data.append("firstName", $("#student-first-name").val());
        data.append("lastName", $("#student-last-name").val());
        data.append("email", $("#student-email").val());
    
        $.ajax({
            type: "POST",
            url: 'php/sessionForm.php',
            data: data,
            contentType: false,
            processData: false,
            success: function(response){
                alert(response.Message);
            }
        });
    });
}

function getTutorAndBind(userId) {
  $.ajax({
      url: './php/getIndividualTutor.php',
      data: { "TutorId": userId },
      dataType: 'json',
      success: function(data)
      {
        tutor = new Tutor(data);
        bindFormElementHandlers();
      }
  });
}

function filterAndGetLoggedInTutorId() {
    $.ajax({
      url: './php/isSignedIn.php',
      data: "",
      dataType: 'json',
      success: function(data)
      {
        if (!data.IsLoggedIn) {
            window.location.href = "SignIn.html";
            return;
        }
        
        if (!data.IsTutor) {
            // Only tutors should be able to submit tutoring session forms
            window.location.href = "Market.html";
            return;
        }
        
        getTutorAndBind(data.UserId);
      }
  });
}

function bindFormElementHandlers() {
    bindClasses();
    bindDate();
    bindRates();
    bindSubmit();
}

function bindDate() {
    var date = new Date();

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (month < 10) {
        month = "0" + month;
    }
    
    if (day < 10) {
        day = "0" + day;
    }

    var today = year + "-" + month + "-" + day;       
    $("#date").val(today);
}

function bindClasses() {
    var classValues = [];
    $.each(tutor.classes, function(index, _class) {
        var id = _class.classId;
        var label = _class.officialIdentifier + ": " + _class.officialName;
        var rate = _class.rate;
        $("#class").append("<option value=" + id + " data-rate=" + rate + ">" + label + "</option>");
    });
}

function bindRates() {
    $("#class").change(function(){
        if ($(this).find("option:selected").value != "base") {
            $("#rate").val($(this).find("option:selected").attr("data-rate"));
            $("#rate").trigger("change");
        }  
    });
    
    $("#num-hours").val(1);
    
    $("#rate,#num-hours").change(function(){
        $("#amount-owed").val($("#rate").val() * $("#num-hours").val());
    });
}

function fixMobileDatePicker() {
    if(!Modernizr.inputtypes.date){
        $('input[type=date]').attr("type", "text");
    }
}

var tutor = null;

(function($) {
  "use strict"; // Start of use strict
  $(function(){
    fixMobileDatePicker();
    filterAndGetLoggedInTutorId();
  });
})(jQuery); // End of use strict